.. _ReleaseNotes:

Release notes
=============

Development version
-------------------

(Significant changes since the last release should be itemized here.)

0.5
---
Release date: February 24, 2017

The code has been significantly restructured since the previous version, in order to increase the modularity; much of the code structure has been changed since v0.4. Specific changes below:

* A parallelization scheme allowing for fast message passing with ZeroMQ.
* A simpler database format based on files, which optionally can be compressed to save diskspace.
* Incorporation of an experimental neural network model based on google's TensorFlow package. Requires TensorFlow version 0.11.0.
* Incorporation of an experimental bootstrap module for uncertainty analysis.

Permanently available at https://doi.org/10.5281/zenodo.322427

0.4
---
Release date: February 29, 2016

Corresponds to the publication of Khorshidi, A; Peterson*, AA. Amp: a modular approach to machine learning in atomistic simulations. Computer Physics Communications 207:310-324, 2016. http://dx.doi.org/10.1016/j.cpc.2016.05.010

Permanently available at https://doi.org/10.5281/zenodo.46737

0.3
---
Release date: July 13, 2015

First release under the new name "Amp" (Atomistic Machine-Learning Package/Potentials).

Permanently available at https://doi.org/10.5281/zenodo.20636


0.2
---
Release date: July 13, 2015

Last version under the name "Neural: Machine-learning for Atomistics". Future versions are named "Amp".

Available as the v0.2 tag in https://bitbucket.org/andrewpeterson/neural/commits/tag/v0.2


0.1
---
Release date: November 12, 2014

(Package name: Neural: Machine-Learning for Atomistics)

Permanently available at https://doi.org/10.5281/zenodo.12665.

First public bitbucket release: September, 2014.
